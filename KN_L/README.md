# Aufgabenstellung - Präsentieren - (Band L)

Am Ende des Projekts sollen Sie den Stakeholdern in einer Präsentation die wichtigsten Aspekte des Projekts aufzeigen.  

Versetzen Sie sich in die Lage dieser Anspruchsgruppe (Stakeholder). Überlegen Sie sich, was diese von Ihnen gerne erfahren würden. Was für sie interessant sein könnte.  

## Tipps

- Starten Sie ausgefallen, bspw. mit einer (rhetorischen) Frage, einem Zitat, mit einem Bild

- Berichten Sie von aufgetretenen Problemen und gefunden Lösungen

- Zeigen Sie die Vorteile Ihres (Lern-)Produkts

- Berichten Sie von positiven und negativen Erfahrungen und Erkenntnissen

- Folien sind nicht unbedingt nötig. Beschränken Sie die Anzahl von Informationen pro Folie. Arbeiten Sie mit Bildern!

- Kurz zusammengefasst: Sagen Sie, worüber Sie sprechen werden. Sprechen Sie darüber und sagen Sie dann, worüber Sie gesprochen haben

- Sie haben schon viele Präsentationen gemacht? Dann versuchen Sie mal Folgendes:
  - Halten Sie sich nicht an Konventionen.
  - Machen Sie keine Bulletpoints.
  - Starten Sie die Präsentation so, wie es niemand erwarten würde.
  - Platzieren Sie nicht mehr als drei Botschaften.
  - Bleiben Sie authentisch.
  - Es braucht einen Start, einen Hauptteil und ein Ende. Zeigen Sie kein Inhaltsverzeichnis, oder haben Sie schon mal einen Film gesehen, bei dem zuerst das Inhaltsverzeichnis gezeigt wird?
  - Schreiben Sie keine Sätze in der Präsentation, die dann auch vorlesen.
  - Bieten Sie eine Show.