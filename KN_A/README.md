# I - Aufgabenstellung - Auftrag entgegennehmen - (Band A)

Hinweis: Sie werden praktisch nie einen Auftrag erhalten, welcher wirklich alle nötigen Angaben enthält. Es ist deshalb wichtig, dass Sie schon früh klären, ob Sie wirklich wissen, was die Auftraggebenden möchten. Vergewissern Sie sich, dass Sie den Auftrag im Sinne der auftraggebenden Personen verstanden haben.

Klären Sie bei allen Begriffen ab, ob Sie diese richtig verstanden haben. Auch Begriffe, welche Sie bereits kennen! Denn: Ein Begriff kann unterschiedliche Bedeutungen haben.

Stellen Sie Fragen zu den Punkten zusammen, welche unklar oder zu wenig präzis formuliert sind.

Stellen Sie sich das Endresultat möglichst genau vor. Je genauer Sie das Resultat schon vor Augen haben, desto genauer wissen Sie, welche Informationen noch fehlen. Und desto schneller kommen Sie ans Ziel. Erklären Sie der auftraggebenden Person, wie Sie den Auftrag verstanden haben.

Sammeln Sie so viele (und nur so viele!) Informationen, wie Sie benötigen, um eine Planung machen zu können. Sie müssen noch nicht alle Details kennen.  
Zitat von Albert Einstein: As simple as possible, but not simpler! 😉