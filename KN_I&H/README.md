# Aufgabenstellung - Kooperation - (Band I & H) 

Nach der Umsetzung und den Tests machen Sie einen Rückblick. Von der ursprünglichen Idee bis zum fertigen Produkt. Sie überlegen sich, was Sie für einen anderen Auftrag mitnehmen. Was gut war, was man besser machen könnte, etc. 

Es hilft, wenn Sie Ihren Blick dabei auf vier Aspekte richten: 

- **Zusammenarbeit**: Beschreiben Sie die Zusammenarbeit, die Kommunikation mit den Beteiligten (Stakeholdern). Wie beurteilen Sie die Verteilung der Arbeiten? Wie schätzen Sie Ihren Beitrag ein? Wie bewerten Sie die Beiträge der anderen Teammitglieder? 

- **Verlauf**: Beschreiben Sie Höhen und Tiefen im Projektverlauf. Wie beurteilen Sie die Planung? Haben sich die getroffenen Entscheide bewährt? 

- **Produkt**: Beschreiben Sie, wie die Anspruchsgruppe (dazu gehören auch Sie) mit der Art und der Qualität des Produkts zufrieden sind. Wurden die Ziele erreicht? 

- **Erfahrungen/Erlerntes**: Welche Erfahrungen machten Sie? Was haben Sie gelernt? Was hat sich bewährt? Was nicht? Was würden Sie beim nächsten Mal anders machen? Welche Dinge nehmen Sie für einen nächsten Auftrag mit? 