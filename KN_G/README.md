# Aufgabenstellung - Qualität sichern - (Band G) 

Die Kundschaft hat diverse Erwartungen an das Endprodukt. Sie möchten diese Erwartungen möglichst gut erfüllen. Darum überlegen Sie schon früh, wie Sie prüfen können, ob diese Ansprüche erfüllt werden.  

Sie erstellen eine Liste von Tests, welche Sie mit dem Produkt durchführen werden. Je früher Sie diese Testfälle festhalten, desto besser. Mit Vorteil bereits in der Planungsphase. So merken Sie schon früh, falls Informationen fehlen.  

Testen können Sie das Produkt natürlich erst, wenn es fertig gestellt ist. Mit den Tests stellen Sie sicher, dass die Qualität Ihres Produkts den Ansprüchen genügt. 

Einige Tipps: 

- Verwenden Sie eine Technik, welche für Ihr Projekt geeignet ist. Manchmal ist eine einfache Checkliste hilfreicher als ein aufwändiges Testprotokoll.

- Wenn Tests fehlschlagen, bedeutet das nicht unbedingt, dass das Produkt unbrauchbar ist. Beurteilen Sie, was die Testresultate über die Qualität des Produkts aussagen.  

- Beim Zusammenstellen der Testfälle hilft ein Trick. Machen Sie Folgendes, bevor Sie einzelne Testfälle aufschreiben: Überlegen Sie sich nur grob, was alles getestet werden kann. Bspw. könnte die Funktion der Buttons getestet werden. Oder die Eingaben. Oder der Druck! Wenn Sie solche Überbegriffe (oder Gruppen) gefunden haben, fallen Ihnen viel mehr Testfälle ein. 