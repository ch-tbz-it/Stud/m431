# Design Prinzipien für die Gestaltung

Wann finden wir eine App *schön*? Oder ein Dokument ist *schön* gestaltet?
Leute, welche diese Dinge *schön* gestalten möchten, beachten gewisse Regeln. Diese Regeln heissen "Design Prinzipien". 

Es gibt eine Unmenge von solchen Prinzipien.
Nachfolgend eine Auswahl von Design Prinzipien aus dem Buch "Die 100 Prinzipien für erfolgreiche Gestaltung", stiebner (2003).

| Kurzname | Beschreibung |
|---|---|
|[Ausrichtung](./Ausrichtung.jpg) |Elemente werden so platziert, dass der Rand von Zeilen oder Spalten in einer Linie ist oder der Text sich an einer Mittelachse orientiert.|
| [Face-ism-Index](./Face-ism-Index.jpg) |Das Gesicht-Körper-Verhältnis bei einem Bild, das die Eindrucksbildung der dargestellten Person beeinflusst.|
| [Klassische Konditionierung](./Klassische%20Konditionierung.jpg)|Technik, bei der ein Reiz mit einer unbewussten physischen oder emotionalen Reaktion verknüpft wird.|
| [Konsistenz](./Konsistenz.jpg) |Die Benutzbarkeit eines Systems wird verbessert, wenn ähnliche Teile auf ähnliche Art ausgedrückt werden.|
| [Affordance](./Affordance.png) |Eigenschaft in der Gestaltung eines Objekts oder einer Umgebung, die beim Benutzer ein bestimmtes, intuitives Verhalten hervorruft. |
| [Archetypen](./Archetypen.png) |Universell gültige Muster von Thema und Form, die aus immanenten Neigungen oder Veranlagungen des Menschen entstehen. |
| [Attraktivitäts-Stereotype](./Attraktivitäts-Stereotype.png) |Attraktive Menschen werden für intelligenter, fähiger, ehrlicher und kontaktfreudiger gehalten als unattraktive. || [Barrierefreiheit](./Barrierefreiheit.png) |Objekte und Umgebungen sollten so gestaltet sein, dass möglichst viele sie ohne Änderungen benutzen können. |
| [Chunking](./Chunking.png) |Technik, bei der viele Informationseinheiten zu einer begrenzten Anzahl von Einheiten oder Chunks zusammengefasst werden, damit man die Information einfacher verarbeitet und sich leichter daran erinnert.|
| [Defensible Space](./Defensible%20Space.png) |Raum mit Markierungspunkten, Überwachungsmöglichkeiten und klaren Angaben zu Verwendung und Besitz.|
| [Fehler](./Fehler.png) |Eine Handlung oder das Unterlassen einer Handlung mit unbeabsichtigten Folgen. |
| [Gemeinsames Schicksal](./Gemeinsames%20Schicksal.png) |Elemente, die sich in dieselbe Richtung bewegen, nimmt man eher als zusammengehörend wahr als solche, die sich in unterschiedliche Richtungen bewegen oder stillstehen.|
| [Gesetz der Geschlossenheit](./Gesetz%20der%20Geschlossenheit.png) |Die Neigung, eine Gruppe von Einzelelementen als erkennbares Muster zu sehen. |
| [Kindchenschema](./Kindchenschema.png) |Die Neigung, Menschen und Dinge mit kindliche wirkenden Gesichtszügen für naiver, hilfloser und ehrlicher zu halten als Menschen mit reifen Gesichtszügen. |
| [Kognitive Dissonanz](./Kognitive%20Dissonanz.png) |Ein Gefühl des Unbehagens aus dem Widerspruch zwischen Ansichten, Gedanken und Meinungen |
| [Konstanz](./Konstanz.png) |Die Neigung, Objekte trotz Änderungen beim sensorischen Input als unverändert anzusehen.|
| [Kontakteffekt](./Kontakteffekt.png) |Wiederholter Kontakt mit Reizen, denen neutrale Gefühle entgegengebracht werden, steigert die Sympathie.|
| [Kosten-Nutzen-Prinzip](./Kosten-Nutzen-Prinzip.png) |Eine Aktivität wird nur weiterverfolgt, wenn der Nutzen daraus grösser oder gleich den Kosten ist. |
| [Verarbeitungstiefe](./Verarbeitungstiefe.png) |Informationen, die eingehend analysiert werden, behält man besser in Erinnerung als nur oberflächlich analysierte Informationen.|
| [Vergleich](./Vergleich.png) |Darstellung von Zusammenhängen und Mustern in einem Systemverhalten mit zwei oder mehr Systemvariablen. |
| [Zugang](./Zugang.png) |Der physikalische oder visuelle Zugang zu einem Design. |
| [Ästhetik und Benutzbarkeit](./Ästhetik%20und%20Benutzbarkeit.png) |Ästhetische Designs werden benutzerfreundlicher empfunden als weniger ästhetische Designs.|