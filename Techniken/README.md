# Techniken
Nachfolgend finden Sie eine Auflistung von verschiedensten Techniken. Techniken sind wie Werkzeuge. Sie helfen Ihnen, eine bestimmte Sache besser zu machen. 

Nicht für alles ist ein Hammer das ideale Werkzeug. Und genau so ist auch eine bestimmte Technik nicht für alles geeignet. 
Für jeden Einsatzart gibt es eine Reihe von Techniken. Wählen Sie jene aus, welche für den Einsatz am besten geeignet ist.

## Kreativitätstechniken
- [Ideenfindungs- und Kreativitätstechniken](https://morethandigital.info/6-erfolgreiche-methoden-fur-brainstorming-ideenfindung/)
- [Kreativitätstechniken - Wikipedia](https://de.wikipedia.org/wiki/Kreativit%C3%A4tstechniken)
- Das Buch der Ideen: [Ideen generieren](./Ressourcen/Kreativitätstechniken%20-%20Das%20Buch%20der%20Ideen%20-%20Ideen%20generieren.pdf)


## Planungstechniken
- [Projektmanagement](https://www.officetimeline.com/de/projektmanagement)
- [Techniken, Methoden, Vorgehensarten](http://openpm.info/) für Projekte


## Entscheidungstechniken
- [Decision Making Guide](https://simplicable.com/new/decision-making)
- [Entscheidungstechniken Brainstorm Wiki](http://wiki.brainstorm-werbung.de/index.php?title=Entscheidungstechniken)<br> 	
ABC-Analyse, Entscheidungstabelle, Entscheidungsbaum, Entscheidungsmatrix, Nutzwertanalyse
- [Nutzwertanalyse](https://projekte-leicht-gemacht.de/blog/pm-methoden-erklaert/nutzwertanalyse/)
- [Entscheidungstechniken für Job und Alltag](./Entscheidungstechniken%20für%20Job%20und%20Alltag.pdf): Wie Sie bessere Entscheidungen treffen
- Entscheidungen treffen und festhalten bei [Software-Architekturen](./Buchauszug%20-%20Software-Architekturen%20-%20Entscheidungen%20treffen%20und%20festhalten.pdf) 


## Präsentationstechniken
- [5 Tipps gegen Lampenfieber](https://www.youtube.com/watch?v=IVVrG8Rrq7c)
- [How to avoid death by Powerpoint](https://www.youtube.com/watch?v=Iwpi1Lm6dFo)
- Eine Reihe von [Videos mit Tipps](https://www.youtube.com/user/moderatorenschulebw/videos)
- [Wirkungsvoll präsentieren](./Ressourcen/Präsentationstechnik%20-%20Wirkungsvoll%20präsentieren%20-%20Gudrun%20Fey.pdf)
- [110 Kommunikations- und Redetechniken](./Ressourcen/The-110-techniques-of-communication-and-public-speaking-David-JP-Phillips.jpg)

## Dokumentationstechniken
- [Aufbau einer Dokumentation](TBZ%20-%20Aufbau%20einer%20Dokumentation.pdf)
- [Design Prinzipien](DesignPrinzipien/README.md) zeigen Ihnen, was *schön* ist

## Lerntechniken
- [Lerntechniken - Brainstorm Wiki](http://wiki.brainstorm-werbung.de/index.php?title=Lerntechniken)


## Gedächtnistechniken
- [4 Merktechniken](https://www.youtube.com/watch?v=FQbtOnHGgyY) (Video)
- [Übersicht](https://de.wikipedia.org/wiki/Kategorie:Mnemotechnik) Gedächtnistechniken
- Interviews mit [Gedächtnisgenies](https://www.youtube.com/playlist?list=PLDgMbN39PyyLtAQgYvz91DbjGwDlqyYHM)