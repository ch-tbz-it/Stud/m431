# Aufgabenstellung - Realisieren - (Band F)

In der Realisierung geht es darum, die geplanten Schritte umzusetzen und das Produkt zu erstellen.  

Geben Sie der auftraggebenden Person Einblick in die Zeit der Realisierung. Stellen Sie sich vor, dass Sie ihr möglichst packend davon erzählen, wie das Ganze abgelaufen ist. 

## Tipps

- Verwenden Sie möglichst viele Bilder (bspw. Screenshots) 

- Beschreiben Sie Ihr Vorgehen

- Beschreiben Sie aufgetretene Probleme, ihre Lösungsversuche und Lösungen

- Beschreiben Sie spezielle Herausforderungen