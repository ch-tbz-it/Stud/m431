# Möglichkeiten finden und auswählen - Band D

## Aufgabenstellung

Für die Erledigung eines Auftrags gibt es meist nicht nur **ein** mögliches Vorgehen, **ein** mögliches Tool oder bspw. **ein** mögliches Design. In solchen Fällen werden in der Planung die Möglichkeiten, welche am meisten Erfolg versprechen, durchgerechnet und miteinander verglichen. Die auftraggebende Person erhält so eine gute Basis für ihre Entscheidung. 

Eine Entscheidung kann nur getroffen werden, wenn vorher abklärt wurde, worauf es überhaupt ankommt, was wichtig ist. Diese Kriterien helfen Ihnen dabei, die möglichen Varianten zu bewerten.   

Erstellen Sie für die wichtigsten Projekt-Entscheide eine Grundlage für die Entscheidung. Verwenden Sie dafür geeignete Entscheidungstechniken. 

Einige Tipps: 

- Nicht alle Informationen (bspw. in einer Entscheidungstabelle) sind gleich wichtig. Machen Sie es der Leserschaft einfach, die wichtigen Informationen möglichst schnell zu finden. Es gibt verschiedene Arten, die Relevanz einer Information auszudrücken: durch die Farbwahl, durch die Grösse, durch die Positionierung, den Abstand etc. 

- Es gibt nicht nur schwarz und weiss: Verwenden Sie Grautöne für Informationen, welche die Leserschaft evtl. wissen möchte, welche aber für den Entscheid weniger wichtig sind. 

- Verwenden Sie Bilder, welche die verschiedenen Varianten repräsentieren.  

- Beispiel einer gelungenen Nutzwertanalyse
![Test Bluetooth-Kopfhörer](../x_gitressourcen/BeispielNutzwertanalyse1024_1.jpg). 
