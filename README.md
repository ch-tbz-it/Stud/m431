![Logo ZH](./x_gitressourcen/ZH_logo.jpg)
# M431 Aufträge im Berufsumfeld selbstständig durchführen

Die Lernenden führen Aufträge aus dem eigenen Berufsumfeld gemäss Vorgaben des Auftraggebers selbständig und mit Hilfe geeigneter Techniken, Methoden und Hilfsmittel durch.

- In der [Modulidentifikation M431](https://www.modulbaukasten.ch/module/431) finden Sie die Vorgaben von ICT zum Modul
- Die Inhalte des Moduls werden im [Skript](M431_Script.pdf) erklärt => [Website](https://ch-tbz-it.gitlab.io/TE/m431_skript/)
- Die Bewertung des Moduls basiert auf dem [M431 Kompetenzraster TBZ](./Kompetenzraster.md)
- [Techniken](./Techniken/README.md) sind unsere Werkzeuge. Sie helfen Ihnen bei der Arbeit.
- [Lernenden-zentriertes Lernen](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-SOL) / SOL (= **s**elbst**o**rganisiertes **L**ernen)
- Die Unterlagen aus der schulübergreifenden Arbeitsgruppe finden sich unter ¨[Modulentwicklung ZH](https://gitlab.com/modulentwicklungzh/cluster-org/m431)
