# Aufgabenstellung - Ressourcen planen - (Band C) 

In einem ersten Schritt wurden grundlegende Fragen zum Auftrag geklärt. Nun gilt es, den Auftrag auf einzelne Tätigkeiten herunterzubrechen.  

Stellen Sie sich das Endprodukt möglichst plastisch vor. Stellen Sie sich vor, wie die auftraggebende Person Ihr Produkt verwendet und sich darüber freut.  

Überlegen Sie sich dann, was alles getan werden muss, damit Sie am Ende das gewünschte Produkt in der gewünschten Qualität vorliegen haben. Erstellen Sie eine Liste dieser Tätigkeiten. Versuchen Sie abzuschätzen, wie lange jede dieser Tätigkeiten benötigen wird (Dauer). Stellen Sie fest, welche Tätigkeiten von anderen Tätigkeiten abhängen. Sortieren Sie die Liste entsprechend um. Weisen Sie diese Tätigkeiten so früh wie möglich jemandem zu (“verantwortlich)”.

Damit Sie den Auftrag umsetzen können, benötigen Sie diverse Hilfsmittel. Man nennt diese Hilfsmittel “Ressourcen”. Erstellen Sie eine Liste der nötigen Ressourcen. Geben Sie an, in welchem Zeitraum die Ressourcen benötigt werden. Vielleicht können Sie auch angeben, für welche Tätigkeiten die Ressourcen benötigt werden.

## Tipps

- Je genauer Sie das endgültige Produkt vor Augen sehen, desto einfacher fällt Ihnen die Planung. Stellen Sie sich vor, wie Sie jemanden erklären, wie Sie das Produkt erstellt haben.

- Bei der Planung teilen Sie eine grosse Aufgabe in kleinere Aufgaben auf. Für die Erledigung jeder dieser Aufgaben könnten Sie grundsätzlich wieder eine andere Person beauftragen. Diese Person würde dann wiederum jede Aufgabe mit IPERKA lösen.  

## Links

### Tools

- [GanttProject](https://www.ganttproject.biz/download)
- [ProjectLibre](http://www.projectlibre.de/)