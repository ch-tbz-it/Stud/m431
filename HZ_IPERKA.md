## Handlungsziele und typische Handlungssituationen zu IPERKA
#### 1. I - Informieren
Ich erhalte einen Auftrag von meinem Chef. Irgendwie geht es um neue PC’s für einen unserer Kunden. Mein Chef hatte mit dem Kunden eine Sitzung und sich daraus Notizen erstellt. Zudem hat unser Kunde offenbar eine Art Pflichtenheft erstellt. Mein Chef gibt mir die ganzen Notizen und Dokumente und sagt mir, ich solle mal einen Vorschlag für eine passende Ersatz PC-Infrastruktur erstellen.
Ich muss mich wohl zuerst einmal informieren, um was es geht.
Checkliste:
-	Ich kenne meinen Auftrag.
-	Ich weiss, was das erwartete Ergebnis ist.
-	Ich weiss, was ich wann fertig haben muss.
-	Ich kenne die Anforderungen (Funktionale Anforderungen, Nichtfunktionale Anforderungen)
-	Ich weiss, wie ich vorgehen werde.
-	Ich habe mich über das Thema zu Auftrag informiert.

#### 2. P - Planen 
Ich habe mich nun  informiert und weiss was ich zu tun habe. Ich kenne den Auftrag.
Nun beginne ich mit der Planung. Dabei zeige ich folgende Punkte auf: 
- was bis wann erledigt sein muss.
- was ich wann tue.
- womit ich das tue.
- wie ich das Ergebnis prüfe.

Dabei müssen auch die Termine berücksichtigt werden:
- ich kenne den End-Termin für meinen Auftrag.
- ich weiss, was vom Auftrag ich bis wann erledigt haben muss. (Meilensteine)

Und auch die Ressourcen spielen eine wichtige Rolle:
- ich weiss, wer mir beim Auftrag zur Seite steht.
- ich weiss, mit welcher Infrastruktur ich den Auftrag erledigen kann.
- ich weiss, wann ich Zeit habe, um am Auftrag zu arbeiten.
- ich weiss, wie der Auftrag finanziert wird, welches Budget mir zur Verfügung steht.

In der Planung wird auch definiert, wie das Ergebnis (in der Phase "Kontrolle") geprüft werden soll. 
- ich weiss, wie Testfälle erstellt werden
- ich weiss, wie ich Testfälle gruppieren kann


#### 3. E - Entscheiden 
Ich habe mich informiert und das Projekt geplant, resp. die Planungsgrössen zusammengetragen. Üblicherweise gibt es mehrere Möglichkeiten für die Umsetzung, weshalb ich mich immer wieder entscheiden muss, was ich wann weshalb wie machen werde. Die Frage ist nur, wie ich mich richtig entscheiden kann. <br>Manchmal hilft es mit kreativen Ansätzen die richtigen Ideen zu finden unter denen man sich dann entschieden kann. Auch kann es hilfreich sein dem Kunden Vorablösungen zu präsentieren (Mockups) und zusammen mit dem Kunden die Entscheidungen zu treffen. Und ganz generell ist es wichtig die Entscheide nachvollziehbar und erklärbar zu treffen.

#### 4. R - Realisieren 
Nachdem alle Entscheidungen getroffen wurden, geht es nun an die Umsetzung. Dabei ist es ratsam sich an die Vorgaben (Zeit, Ressourcen, Arbeitspakete etc.) zu halten und die geleistete Arbeit mittels Arbeitsrapporten festzuhalten. Sie bilden die Basis für die Abrechnung und die spätere Kontrolle des Projektstandes. <br>Wichtig ist auch, dass dem Auftraggeber der Projektstand regelmässig präsentiert wird und für alle Beteiligten der Projektstand transparent dargelegt wird. In den wenigsten Fällen erfolgt die Realisierung alleine, sondern vielmehr als Teamarbeit, was weitere Kompetenzen erfordert.  

#### 5. K - Kontrollieren 
Um sicher zu sein, dass das was ich oder wir realisiert haben, muss ich die Resultate laufend kontrollieren. Idealerweise erfolgt die Kontrolle nicht einfach willkürlich, sondern auf eine Art und Weise, dass sie wiederholbar und nachvollziehbar ist. <br>Ich mache mir also schon weit vor der Testphase Gedanken darüber, wie ich die Resultate kontrolliere. Dies geschieht bereits bei der Planung und Entscheidungsphase weshalb es auch wichtig ist möglichst messbare Entscheidungskritierien und Planungsgrössen zu haben. Nur was messbar ist, kann später auch auf seine Erfüllung hin kontrolliert werden.

#### 6. A - Auswerten 
Nachdem die Resultate der Kontrollen vorliegen, muss ich diese auswerten und allenfalls Anpassungen bei der Realisierung, den Testfällen oder beim eigentlichen Testen (also beim Durchführen der Kontrollen) vornehmen. <br>Ich analysiere den Verlauf des Projektes, das Verhalten des Teams, das eigene Verhalten und gewinne so Erkenntnisse für das nächste Projekt.
Diesen Prozess des prüfenden Nachdenkens bezeichnet man als Reflexion.


