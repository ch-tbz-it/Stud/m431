# Aufgabenstellung - Auftrag erfassen - (Band J)

Erstellen Sie einen Projektauftrag, in welchem Sie die wichtigsten Informationen zum Projekt festhalten.  

Nicht jede Kundschaft ist fähig, einen Projektauftrag korrekt aufzusetzen. Sie als Fachkraft können dies in Absprache mit der auftraggebenden Person erledigen.  

## Einige Tipps

- Welche Angaben in einen Projektauftrag gehören, können Sie [Wikipedia](https://de.wikipedia.org/wiki/Projektauftrag#Bestandteile) entnehmen. Schreiben Sie am besten alle Angaben in eine Tabelle.

- Wählen Sie als Kurzbezeichnung eine wirklich kurze, knackige Bezeichnung. Oder vielleicht auch eine Abkürzung. Am besten nur ein Wort/Begriff. 
Hinweis: Verwenden Sie diese Bezeichnung bspw. in Dateinamen, im Betreff von E-Mails, etc. So ist gleich klar, wozu die Datei/das E-Mail gehört.

- Schreiben Sie kurz, knapp (keine Prosa)  

- Definieren Sie Meilensteine. Auf diese Daten freut sich die auftraggebende Person (weil sie was zu sehen bekommt) und Sie als auftragnehmende Person (weil Sie etwas zu feiern haben).

- Überlegen Sie sich genau, was die auftraggebende Person als Ergebnis erhält. Ist es ein Zip-File mit allen Sources? Oder ein Video im Format MP4? Oder eine MP3-Audiodatei? Oder die vollständige Planung für ein Konzert? Was ist also der “Output” des Projekts?

- Die Ziele sollten SMART definiert sein: spezifisch, messbar, akzeptiert, realistisch und terminiert.

- Ein Projekt kann sehr viel Geld kosten. Deshalb braucht es eine gute Begründung, weshalb das Projekt nötig ist. Unter Unternehmensbedarf geben Sie diese Gründe an.

- Wenn etwas zum Zeitpunkt, an welchem der Projektauftrag entsteht, noch nicht geklärt werden kann, müssen Annahmen getroffen und beschrieben werden.

- Beschränkungen setzen dem Projektteams (bspw. finanzielle, zeitliche, etc.) Grenzen/Limiten.

- Abgrenzungen definieren, worum sich das Projekt explizit nicht kümmern soll.

## Links

- [Methoden, um Projektnamen zu generieren](https://www.timr.com/a/projektnamen-generieren/)
- [Bestandteile eines Projektauftrags](https://de.wikipedia.org/wiki/Projektauftrag#Bestandteile)