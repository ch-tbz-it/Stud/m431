# Aufgabenstellung - Projektdokumentation - (Band K)

Der Ablauf des Projekts soll für die auftraggebende Person nachvollziehbar sein. Deshalb erstellen Sie parallel zur Umsetzung des Auftrags eine Dokumentation, in welcher Sie die verschiedenen Schritte festhalten.  

## Tipps

- Titelblatt: Knackiger Titel, hilfreicher Untertitel, Autoren, Datum, Kontext (Modul), Bild

- Kopf-/Fusszeilen sinnvoll verwenden (bspw. für Seitenangabe, Projektname)

- Inhaltsverzeichnis klar strukturiert (bspw. nach den Schritten von IPERKA)

- Kein Titel ohne mindestens einen einführenden Satz.

- Versehen Sie Tabellen mit Kopfzeilen. Arbeiten Sie mit (wenigen) Farben

- Verwenden Sie dezimalnummerierte Titel (dies vereinfacht die Referenzierung)

- Beschriften Sie alle Bilder (Sie erhalten dadurch sehr einfach ein Abbildungsverzeichnis)

- Erstellen Sie ein Quellenverzeichnis

- Beachten Sie Grammatik, Rechtschreibung und Interpunktion (Komma, Punkte)


