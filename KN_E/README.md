# Aufgabenstellung - Lernjournal - (Band E) 

Im Lernjournal geben Sie Hinweise über die geleistete Arbeit, aufgetauchte Probleme, verwendete Ressourcen, die Strategien zur Lösungssuche, etc. 

## Tipps

Ins Lernjournal könnten Sie schreiben, ... 

- was Sie getan haben 

- was Sie gelernt haben 

- wo Sie noch Probleme haben / was Sie nicht verstanden haben 

- was hilfreich war 

- was hinderlich war 

- wie Sie ein Problem angingen und vielleicht lösen konnten (Vorgehen) 

- was Sie gut gemacht haben, was verbessert werden könnte 

- welche Fragen Sie noch offen haben 

- was Sie als nächstes tun werden 

- etc. 